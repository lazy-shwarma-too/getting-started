# Text manipulation:
    - awk
    - sed
    - grep
        - REGEX, because it is really basics for text manipulation
    - uniq
    - sort
    - echo
    - printf
    - tr
    - join
    - cat
    - cut
    - head
    - tail
    - less/more
    - tee
    - diff
    - comm
    - strings
