
# VIM

- Introduction to vi  2
    - command mode and insert mode  3
    - start typing (a A i I o O)  3
    - replace and delete a character (r x X)  
    - undo and repeat (u )  
    - cut, copy and paste a line (dd yy p P)  
    - cut, copy and paste lines (3dd 2yy)  
    - start and end of a line (0 or ^ and $)  
    - join two lines (J) and more 
    - words (w b)
    - save (or not) and exit (:w :q :q! )  
    - searching (/ ?) 
    - replace all ( :1,$ s/foo/bar/g ) 
    - reading files (:r :r !cmd)  
    - text buffers 
    - multiple files 
    - abbreviations  
    - key mappings  
    - setting options
